<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SchoolAdmission@create');
Route::post('/myproject/list', 'SchoolAdmission@store');
Route::post('/myproject/list', 'SchoolAdmission@list');
Route::get('/myproject/{data}/student', 'SchoolAdmission@hello');
Route::get('/myproject/{data}/edit', 'SchoolAdmission@edit');
Route::patch('/myproject/{data}/student', 'SchoolAdmission@update');

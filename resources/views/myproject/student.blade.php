@extends('layout')

@section('content')
<div class="table">
    <br>
    <table
        style='width: 100%; position: relative; border:1px solid black; border-collapse:collapse; text-align:center;'>
        <tr>
            <th style='border:1px solid black; border-collapse:collapse;'>ID</th>
            <th style='border:1px solid black; border-collapse:collapse;'>FirstName</th>
            <th style='border:1px solid black; border-collapse:collapse;'>StudentLastName</th>
        </tr>
        <br>
        @foreach ($data as $d)
        <tr>
            <td style='border:1px solid black; border-collapse:collapse;'>{{ $d->id }}</td>
            <td style='border:1px solid black; border-collapse:collapse;'>{{ $d->first_name }}</td>
            <td style='border:1px solid black; border-collapse:collapse;'>{{ $d->last_name }}
                <a href="/myproject/{{$d->id}}/edit" style="position:absolute; right: 60px;">Edit</a>
            </td>
        </tr>
        @endforeach
</div>
@endsection
@extends('layout')
@section('content')
<style>
    body {
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }

    a {
        font-size: 25px;
        color: black;
    }
</style>
@foreach ($school as $schools)
<ul style="list-style-type: none;">
    <li>
        <a href="/myproject/{{ $schools->id }}/student">{{ $schools->id }}.{{ $schools->name }}</a>
    </li>
</ul>
@endforeach
@endsection
@extends('layout')

@section('content')

<link rel="stylesheet" type="text/css" href=" {{ asset('css/style.css') }}">
<h1 style="text-align:center; font-size: 50px;">School Admission System</h1>
<div>
    <form action="/myproject/list" method="POST">
        {{ csrf_field() }}
        <br>
        <select name="grades">
            <option value="" selected disabled>Choose Grade</option>
            @foreach ($school as $schools)
            <option value="{{ $schools->id }}">{{ $schools->name }}</option>
            @endforeach
        </select>
        <input type="text" name="s_name" placeholder="Enter Student Name" required /><br>
        <input type="text" name="s_last_name" placeholder="Enter Student LastName" required />
        <input type="submit" name="submittion" value="Submit" autofocus="false">
    </form>
</div>
@endsection
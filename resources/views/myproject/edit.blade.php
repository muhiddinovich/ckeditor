@extends('layout')

@section('content')
<form method="POST" action="/myproject/{{ $data->id }}/student">
    @method('PATCH')
    {{ csrf_field() }}
    <div class="input">
        <input type="text" name="f_n" required placeholder="Enter Name : " value="{{ $data->first_name }}"><br>
        <input type="text" name="l_n" required placeholder="Enter Last Name :" value="{{ $data->last_name }}"><br>
        <input type="submit" value="Submit" name="Submit">
    </div>
</form>
@endsection
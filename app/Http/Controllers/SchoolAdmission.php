<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\grade;
use App\data;

class SchoolAdmission extends Controller
{
    public function create()
    {
        $school = grade::all();

        return view('myproject.create', compact('school'));
    }
    public function store()
    {
        $data = new data();

        $data->grade_id = request('grades');
        $data->first_name = request('s_name');
        $data->last_name = request('s_last_name');

        $data->save();
    }
    public function list()
    {
        $this->store();

        $data = data::all();
        $school = grade::all()->where('name');

        return view('myproject.list', compact('data', 'school'));
    }
    public function hello($grade_id)
    {
        $data = data::where('grade_id', $grade_id)->get();
        //dd($data);

        return view('myproject.student', compact('data'));
    }
    /*public function table($id)
    {
        $this->hello($id);
        $data = data::findOrFail($id);
        //$school = grade::all();

        return view('myproject.student', compact('data'));
    }*/
    public function edit($id)
    {
        $data = data::findOrFail($id);

        return view('myproject.edit', compact('data'));
    }
    public function update($id)
    {
        $data = data::findOrFail($id);

        $data->first_name = request('f_n');
        $data->last_name = request('l_n');

        //dd($data);
        $data->save();
        return redirect('/myproject/' . $data->grade_id . '/student');
    }
}
